#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
import io
import os
import sys
import unittest

import compra

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

expected = ["""Elemento a comprar: Lista de la compra:
patatas
leche
pan
Elementos habituales: 3
Elementos específicos: 0
Elementos en lista: 3
""",
"""Elemento a comprar: Elemento a comprar: Lista de la compra:
patatas
leche
pan
chocolate
Elementos habituales: 3
Elementos específicos: 1
Elementos en lista: 4
""",
"""Elemento a comprar: Elemento a comprar: Elemento a comprar: Lista de la compra:
patatas
leche
pan
chocolate
mantequilla
Elementos habituales: 3
Elementos específicos: 2
Elementos en lista: 5
""",
"""Elemento a comprar: Elemento a comprar: Elemento a comprar: Elemento a comprar: Lista de la compra:
patatas
leche
pan
chocolate
mantequilla
Elementos habituales: 3
Elementos específicos: 3
Elementos en lista: 5
""",
"""Elemento a comprar: Elemento a comprar: Elemento a comprar: Elemento a comprar: Elemento a comprar: Lista de la compra:
patatas
leche
pan
chocolate
mantequilla
Elementos habituales: 3
Elementos específicos: 4
Elementos en lista: 5
""",
]


class TestOutput(unittest.TestCase):

    def test_simple(self):
        stdout = io.StringIO()
        sys.stdin = io.StringIO("\n")
        with contextlib.redirect_stdout(stdout):
            compra.main()
            output = stdout.getvalue()
        self.assertEqual(expected[0], output)

    def test_elemento(self):
        stdout = io.StringIO()
        sys.stdin = io.StringIO("chocolate\n\n")
        with contextlib.redirect_stdout(stdout):
            compra.main()
            output = stdout.getvalue()
        self.assertEqual(expected[1], output)

    def test_elementos(self):
        stdout = io.StringIO()
        sys.stdin = io.StringIO("chocolate\nmantequilla\n\n")
        with contextlib.redirect_stdout(stdout):
            compra.main()
            output = stdout.getvalue()
        self.assertEqual(expected[2], output)

    def test_elementos_repe(self):
        stdout = io.StringIO()
        sys.stdin = io.StringIO("chocolate\nmantequilla\nchocolate\n\n")
        with contextlib.redirect_stdout(stdout):
            compra.main()
            output = stdout.getvalue()
        self.assertEqual(expected[3], output)

    def test_elementos_repe2(self):
        stdout = io.StringIO()
        sys.stdin = io.StringIO("chocolate\nleche\nmantequilla\nchocolate\n\n")
        with contextlib.redirect_stdout(stdout):
            compra.main()
            output = stdout.getvalue()
        self.assertEqual(expected[4], output)


class TestTypes(unittest.TestCase):

    def test_habitual(self):
        self.assertIsInstance(compra.habitual, tuple)


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
