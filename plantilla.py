#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''
habitual = ("patatas", "leche", "pan")
def main():

    especifico = []

    while True:
       elemento= input("Elemento a comprar: ")
       if elemento == "":
            break
       especifico.append(elemento)

    lista = list(set(list(habitual) + especifico))
    print('Lista de la compra: ')
    for elemento in lista:
        print(elemento)

    elementos_habituales = len(habitual)
    elementos_especificos = len(especifico)
    elementos_totales = len(lista)

    print("Elementos habituales:", elementos_habituales)
    print("Elementos especificos:", elementos_especificos)
    print("Elementos en lista:", elementos_totales)

if __name__ == '__main__':
    main()


